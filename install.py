import os
import tkinter as tk
from tkinter import filedialog as f
from tkinter import messagebox
import shutil
import textwrap

ifp = None
fp = os.path.abspath('.')
itk = os.system('pip3 install tkinter')
ipt = os.system('pip3 install pint')

install = tk.Tk()
install.title("单位转换器安装程序")
install.geometry("300x200")
install.resizable(False, False)
hello = tk.Label(install, text="欢迎使用安装程序", wraplength=0)
hello.pack()
hello.place(x=60)


def cf():
    global ifp
    global fp
    if os.path.exists(ifp):
        response = messagebox.askyesno("目标目录已存在", "目标目录已存在，是否覆盖？")
        if response:
            shutil.rmtree(ifp)
        else:
            messagebox.showinfo("操作取消", "用户取消操作")
            install.destroy()
            exit()

    try:
        shutil.copytree(fp, ifp, dirs_exist_ok=True)
        os.remove(ifp + "/install.py")
        messagebox.showinfo("安装成功", "单位转换器安装成功")
        run = messagebox.askyesno("结束安装程序", "是否运行单位转换器？")
        if run == 1:
            os.system("pythou3 -m " + ifp + "/main.py")
        exit()
    except Exception as e:
        messagebox.showerror("安装失败", f"发生错误: {e}")


def cp():
    global ifp
    global fp
    ifp = f.askdirectory(title="请选择安装文件夹")
    while not ifp:
        messagebox.showinfo("选择安装文件夹", "请重新选择")
        ifp = f.askdirectory(title="请选择安装文件夹")
    n1.destroy()
    hello.destroy()
    wrapped_text = textwrap.fill(ifp, width=15)
    sfp = tk.Label(install, text="安装目录：" + wrapped_text)
    sfp.pack()

    def inp():
        global n2
        global cbuntOne
        global sfp
        if ccb:
            os.system("pip3 install numpy")
        install.destroy()
        cf()

    n2 = tk.Button(install, text="下一步(next)>>", command=inp)
    n2.pack()
    n2.place(x=110, y=150)
    ccb = False

    def cc():
        global ccb
        if varone.get() == 1:
            ccb = True
        else:
            ccb = False

    varone = tk.IntVar()
    cbutone = tk.Checkbutton(install, text="安装NumPy(拓展功能）", variable=varone, command=cc)
    cbutone.pack()


n1 = tk.Button(install, text="下一步(next)>>", command=cp)
n1.pack()
n1.place(x=110, y=150)
install.mainloop()